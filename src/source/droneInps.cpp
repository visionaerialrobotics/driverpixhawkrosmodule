//Drone
#include "droneInps.h"

// The commands comming from the midlevel controller needs to be muliplied by these values
// to covert the thrust commands in the range of -1 to +1
static double THRUST_SCALE_FULL_BATTERY = 1/32.681;
static double THRUST_SCALE_HALF_BATTERY = 1/29.71;
static double THRUST_SCALE_LOW_BATTERY  = 1/27.43;

////// DroneCommand ////////
DroneCommandROSModule::DroneCommandROSModule() :
    DroneModule(droneModule::active)
{
    init();
    return;
}

DroneCommandROSModule::~DroneCommandROSModule()
{
    return;
}

void DroneCommandROSModule::init()
{
    return;
}

void DroneCommandROSModule::close()
{
    return;
}

void DroneCommandROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //Publishers
    drone_CtrlInput_publisher       = n.advertise<mavros_msgs::AttitudeTarget>("mavros/setpoint_raw/attitude", 1, true);
    mavros_yaw_command_publisher    = n.advertise<geometry_msgs::Vector3Stamped>("mavros/yaw_command",1,true);

    //Subscribers
    ML_autopilot_command_subscriber = n.subscribe("command/low_level", 1, &DroneCommandROSModule::MLAutopilotCommandCallback, this);
    rotation_angles_subscriber      = n.subscribe("rotation_angles", 1, &DroneCommandROSModule::rotationAnglesCallback, this);
    battery_subscriber              = n.subscribe("battery",1, &DroneCommandROSModule::batteryCallback, this);
    hector_slam_subscriber          = n.subscribe("odometry/filtered", 1, &DroneCommandROSModule::hectorSlamCallback, this);
    yaw_ref_subscriber              = n.subscribe("droneControllerYawRefCommand", 1, &DroneCommandROSModule::yawReferenceCallback, this);
    Ekf_yaw_subscriber              = n.subscribe("EstimatedPose_droneGMR_wrt_GFF",1,&DroneCommandROSModule::ekfyawCallback, this);

    //Initialization
    yaw_angle = 1E10; //High value
    yaw_initial_mag = 1E10; //High value
    yawRef = 0;     //Current yaw
    update = true;

    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool DroneCommandROSModule::resetValues()
{
    return true;
}

//Start
bool DroneCommandROSModule::startVal()
{
    return true;
}

//Stop
bool DroneCommandROSModule::stopVal()
{
    return true;
}

//Run
bool DroneCommandROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

void DroneCommandROSModule::yawReferenceCallback(const droneMsgsROS::droneYawRefCommand& msg){
    yawRef = msg.yaw;
    update = true;
}

#define KEEP_IN_RANGE(a, min, max) if (a < min) a = min; else if (a > max) a = max;

void DroneCommandROSModule::MLAutopilotCommandCallback(const droneMsgsROS::droneAutopilotCommand& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    mavros_msgs::AttitudeTarget    attitude_msg;
    geometry_msgs::Vector3Stamped  yaw_command_msg;

    attitude_msg.header.stamp = ros::Time::now();
    yaw_command_msg.header.stamp = ros::Time::now();

//    timePrev = timeNow;
//    timeNow = (double) ros::Time::now().sec + ((double) ros::Time::now().nsec / (double) 1E9);

    yaw_command = (yaw_angle + (msg.dyaw * (0.06) )) * (M_PI/180.0); // Control the angle precisely

    // Debug
    //yaw_command = 90;

    yaw_command_msg.vector.z = yaw_command;

    if(yaw_command_msg.vector.z <=2*M_PI && yaw_command_msg.vector.z > M_PI)
        yaw_command_msg.vector.z = yaw_command_msg.vector.z - 2*M_PI;
    else if(yaw_command_msg.vector.z >=0 && yaw_command_msg.vector.z <= M_PI)
        yaw_command_msg.vector.z = yaw_command_msg.vector.z;

    yaw_command_msg.vector.z = - yaw_command_msg.vector.z;

    //std::cout << "Controlling yaw again with dYaw" << "  yaw_angle: " << yaw_angle * (M_PI/180.0) << "  dYaw: " << (msg.dyaw * (timeNow - timePrev)) * (M_PI/180.0) << std::endl;


    //convert the values in radians
    roll_command  = (+1)*msg.roll*(M_PI/180.0);
    pitch_command = (+1)*msg.pitch*(M_PI/180.0);
//  yaw_command   = (+1)*yaw_command *(M_PI/180.0);
    //std::cout << "yaw_command " <<  yaw_command  << std::endl;

    //convert euler angles to quaternion but in eigen form
    quaterniond =  mavros::UAS::quaternion_from_rpy(roll_command, pitch_command, yaw_command);

    //converting the NED frame to ENU for mavros
    quaterniond_transformed = mavros::UAS::transform_orientation_ned_enu(mavros::UAS::transform_orientation_baselink_aircraft
                                                                         (quaterniond));

    //convert quaternion in eigen form to geometry_msg form
    tf::quaternionEigenToMsg(quaterniond_transformed, orientation);


    attitude_msg.orientation = orientation;

    //choose the thrust values depending on the battery percentage
    //    if(battery_percent <= 100.0 && battery_percent >= 50.0)
    //       //converting thrust in the range of 0 to +1
    //      attitude_msg.thrust = msg.thrust * THRUST_SCALE_FULL_BATTERY;
    //    else if(battery_percent < 50.0 && battery_percent >= 0.0)
    //      attitude_msg.thrust = msg.thrust * THRUST_SCALE_HALF_BATTERY;
    //    else if(battery_percent < 0 && battery_percent >= -20.0)
    //      attitude_msg.thrust = msg.thrust * THRUST_SCALE_LOW_BATTERY;
    //    else
    attitude_msg.thrust = msg.thrust * THRUST_SCALE_HALF_BATTERY;

    drone_CtrlInput_publisher.publish(attitude_msg);
    mavros_yaw_command_publisher.publish(yaw_command_msg);

    return;
}

void DroneCommandROSModule::rotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg)
{
    yaw_angle = msg.vector.z;
    return;
}

void DroneCommandROSModule::batteryCallback(const droneMsgsROS::battery &msg)
{
    battery_percent = msg.batteryPercent;
    return;
}

void DroneCommandROSModule::hectorSlamCallback(const nav_msgs::Odometry &msg)
{
    //    geometry_msgs::PoseStamped vision_pose;

    //    vision_pose.header.stamp      = ros::Time::now();
    //    vision_pose.header.frame_id   = "odom";
    //    vision_pose.pose.position     = msg.pose.pose.position;
    //    vision_pose.pose.orientation  = msg.pose.pose.orientation;

    //    mavros_pose_publisher.publish(vision_pose);
}

void DroneCommandROSModule::ekfyawCallback(const droneMsgsROS::dronePose &msg)
{
    ekf_yaw = msg.yaw;
    if(ekf_yaw < 0) ekf_yaw += 2*M_PI;
}
