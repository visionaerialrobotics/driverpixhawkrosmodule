//////////////////////////////////////////////////////
//  rotationAnglesNode.cpp
//
//  Created on: Dec 19, 2015
//      Author: Hriday Bavle
//
//  Last modification on: Dec 19, 2015
//      Author: Hriday Bavle
//
//////////////////////////////////////////////////////



//I/O stream
//std::cout
#include <iostream>


//ROS
#include "ros/ros.h"

//Pixhawkouts
#include "droneOuts.h"

//Comunications
#include "communication_definition.h"


using namespace std;

int main(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, "droneLocalPose");
    ros::NodeHandle n;

    cout<<"[ROSNODE] Starting droneLocalPose"<<endl;

    //Vars
    LocaPoseROSModule MyLocaPoseROSModule;
    MyLocaPoseROSModule.open(n,"droneLocalPose");

    try
    {
        //Read messages
        ros::spin();
        return 1;

    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }
}
